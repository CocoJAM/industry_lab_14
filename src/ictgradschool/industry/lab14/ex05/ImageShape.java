package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;
    private SwingWorker<Image, Image> worker;

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);
        worker = new SwingWorker<Image, Image>() {

            @Override
            protected Image doInBackground() throws Exception {
                Image sample1 = null;
                try {
                    Image imageread = ImageIO.read(url);
                    if (width == imageread.getWidth(null) && height == image.getHeight(null)) {
                        sample1 = imageread;
                    } else {
                        sample1 = imageread.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                        sample1.getWidth(null);
                    }
                } catch (IOException e) {

                    e.printStackTrace();
                }
                return sample1;
            }

            @Override
            public void done() {
                try {
                    System.out.println("Done");
                    image = get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            }
        };


    }


    @Override
    public void paint(Painter painter) {
        worker.execute();
        if (!worker.isDone()){
            painter.fillRect( fX, fY, fWidth, fHeight);
        }
        else {
        painter.drawImage(this.image, fX, fY, fWidth, fHeight);}
    }

}
