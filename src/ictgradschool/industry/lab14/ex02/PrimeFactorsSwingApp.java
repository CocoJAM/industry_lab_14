package ictgradschool.industry.lab14.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by ljam763 on 2/05/2017.
 */
public class PrimeFactorsSwingApp extends JPanel {

    private JButton _startBtn;
    private JButton _endBtn;   // Button to start the calculation process.
    private JTextArea _factorValues;  // Component to display the result.
    private final JTextField tfN = new JTextField(20);

    class calculation extends SwingWorker<List<Long>, Void>{

        @Override
        protected List<Long> doInBackground() throws Exception {
            String strN = tfN.getText().trim();
            long n = 0;
            List<Long> list = new ArrayList<>();
            try {
                n = Long.parseLong(strN);
            } catch(NumberFormatException e) {
                System.out.println(e);
            }

            // Disable the Start button until the result of the calculation is known.

            // Clear any text (prime factors) from the results area.
//            _factorValues.setText(null);

            // Set the cursor to busy.
//            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            // Start the computation in the Event Dispatch thread.
            for (long i = 2; i*i <= n; i++) {

                // If i is a factor of N, repeatedly divide it out
                while (n % i == 0) {
                    list.add(i);
//                    _factorValues.append(i + "\n");
                    n = n / i;
                }
            }

            // if biggest factor occurs only once, n > 1
            if (n > 1) {
                list.add(n);
//                _factorValues.append(n + "\n");
            }

            return list;
        }


        @Override
        protected void done(){
//                 Re-enable the Start button.
                        _factorValues.setText(null);
                        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            _startBtn.setEnabled(true);
            try {
                List list =get();
                for (Object o : list) {
                    _factorValues.append(o + "\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            // Restore the cursor.
            setCursor(Cursor.getDefaultCursor());
        }
    }


    public PrimeFactorsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");

        _startBtn = new JButton("Compute");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);




        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                calculation asd = new calculation();
                _startBtn.setEnabled(false);
                asd.execute();
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500,300));
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("PrimeFactorsSwingApp");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new PrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
